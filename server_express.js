const express = require('express')
const app = express();

app.get('/', (req, res) => {
    res.send(
        `
        <html>
        <body>
        Hello, valera!
</body>
</html>
        `
    )
})

app.get('/user', (req, res) => {
    res.send(JSON.stringify({
        name: 'Valera',
        lastName: 'Valera'
    }))
})

//params

app.get('/user/:id', (req, res) => {
    const id = req.params.id
    res.send(
        `
        <html>
        <body>
        User: ${id}
</body>
</html>
        `
    )
})

//query strings
app.get('/car', (req, res) => {

    let car = req.query.car
    let type = req.query.type

    res.send(
        `
        <html>
        <body>
        User: ${car}, type: ${type}
</body>
</html>
        `
    )
})



const port = process.env.PORT || 3000

app.listen(port)

