const http = require('http')
const fs = require('fs')


http.createServer((req, res) => {

    if (req.url === '/') {
        let html = fs.readFileSync('./index.html')
        res.writeHead(200, {'Content-type': 'text/html'})
        res.end(html)

    } else if (req.url === '/user') {
        res.writeHead(200, {'Content-type': 'application/json'})
        const json = JSON.stringify({
            name : "Valera",
            car : ["zapor", "nissan"]
        })
        res.end(json)
    } else {
        res.writeHead(404)
        res.end()
    }




}).listen(8181, '127.0.0.1');